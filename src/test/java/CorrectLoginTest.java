import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

public class CorrectLoginTest extends SeleniumBaseTest {

    @Test
    public void correctLoginTest (){
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .assertWelcomeElmIsShown ();
    }

}
