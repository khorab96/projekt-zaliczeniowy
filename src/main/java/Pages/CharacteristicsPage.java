package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage {

    public CharacteristicsPage ( WebDriver driver ) {
        super ( driver );
    }

    @FindBy(css = ".title_left")
    private WebElement characteristicsPageTitle;


    public CharacteristicsPage assertIfCharacteristicsPageIsOpen ( ) {
        Assert.assertEquals ( driver.getCurrentUrl ( ), "http://localhost:4444/Characteristics", "this is not a characteristics page" );
        return this;
    }

}
