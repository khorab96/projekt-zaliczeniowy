import Pages.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.UUID;

public class UserRegistrationWithPwNotMeetingRequirementsTest extends SeleniumBaseTest {

    String userEmail = UUID.randomUUID ( ).toString ( ).substring ( 0, 10 ) + "@test.com";

    @DataProvider
    public static Object[][] wrongPasswords ( ) {
        return new Object[][]{
                {"Test!!", "Passwords must have at least one digit ('0'-'9')."},
                {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Test11", "Passwords must have at least one non alphanumeric character."}
        };
    }


    @Test(dataProvider = "wrongPasswords")
    public void IncorrectUserRegistrationTest ( String password, String expErrorMsg ) {

        new LoginPage ( driver )
                .clickRegisterLink ( )
                .typeEmail ( userEmail )
                .typePassword ( password )
                .typeConfirmPassword ( password )
                .clickRegisterBtnWithFailure ( )
                .assertPwErrorMsgIsShown ( expErrorMsg );
    }
}


