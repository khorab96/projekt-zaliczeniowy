package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class RegistrationPage extends LoginPage {

    public RegistrationPage ( WebDriver driver ) {
        super ( driver );
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "#ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(css = "button[type='submit']")
    private WebElement registerBtn;

    @FindBy(id = "Email-error")
    private WebElement emailError;

    @FindBy(css = "#ConfirmPassword-error")
    private WebElement pwNotMatchError;

    @FindBy(css = ".validation-summary-errors>ul>li")
    private List<WebElement> pwErrors;


    public RegistrationPage typeEmail ( String email ) {
        emailTxt.clear ( );
        emailTxt.sendKeys ( email );
        return this;
    }

    public RegistrationPage typePassword ( String password ) {
        passwordTxt.clear ( );
        passwordTxt.sendKeys ( password );
        return this;
    }

    public RegistrationPage typeConfirmPassword ( String confirmPassword ) {
        confirmPasswordTxt.clear ( );
        confirmPasswordTxt.sendKeys ( confirmPassword );
        return this;
    }

    public HomePage clickRegisterBtnWithSuccess ( ) {
        registerBtn.click ( );
        return new HomePage ( driver );
    }

    public RegistrationPage clickRegisterBtnWithFailure ( ) {
        registerBtn.click ( );
        return this;
    }

    public RegistrationPage assertIfPasswordNotMatchErrorIsShown ( ) {
        Assert.assertEquals ( pwNotMatchError.getText ( ), "The password and confirmation password do not match." );
        System.out.println ( pwNotMatchError.getText ( ) );
        return this;
    }

    public RegistrationPage assertPwErrorMsgIsShown ( String errorMsg ) {
        Assert.assertEquals ( loginErrors.get ( 0 ).getText ( ), errorMsg );
        return this;
    }


}