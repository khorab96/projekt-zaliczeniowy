import Pages.LoginPage;
import org.testng.annotations.Test;

import java.util.UUID;

public class UserRegistrationWithPasswordsNotMatchingTest extends SeleniumBaseTest {

    String userEmail = UUID.randomUUID ( ).toString ( ).substring ( 0, 10 ) + "@test.com";

    // different password and Confirmed password
    @Test
    public void IncorrectUserRegistrationTest ( ) {

        new LoginPage ( driver )
                .clickRegisterLink ( )
                .typeEmail ( userEmail )
                .typePassword ( "Test1!" )
                .typeConfirmPassword ( "Test!!!!" )
                .clickRegisterBtnWithFailure ( )
                .assertIfPasswordNotMatchErrorIsShown ( );
    }

}
