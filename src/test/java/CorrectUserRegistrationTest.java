import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

import java.util.UUID;

public class CorrectUserRegistrationTest extends SeleniumBaseTest {


    String userEmail = UUID.randomUUID ( ).toString ( ).substring ( 0, 10 ) + "@test.com";
    String password = new Config ( ).getApplicationPassword ( );

    @Test
    public void correctUserRegistrationTest ( ) {
        new LoginPage ( driver )
                .clickRegisterLink ( )
                .typeEmail ( userEmail )
                .typePassword ( password )
                .typeConfirmPassword ( password )
                .clickRegisterBtnWithSuccess ( )
                .assertWelcomeElmIsShown ( );
    }
}
