import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

public class AddIncorrectProcessTest extends SeleniumBaseTest {

    @Test
    public void addIncorrectProcessTest ( ) {
        String shortProcessName = "ab";
        String expMsg = "The field Name must be a string with a minimum length of 3 and a maximum length of 30.";
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToProcesses ( )
                .assertIfProcessesPageIsOpen ( )
                .goToCreateProcessPage ( )
                .typeProcessName ( shortProcessName )
                .typeProcessDescription ( "test" )
                .typeProcessNotes ( "test" )
                .submitProcessFailure ( )
                .assertIfErrorAppeared ( expMsg )
                .gotBackToProcessesPage ( )
                .assertProcessIsNotShown ( shortProcessName );

    }

}
