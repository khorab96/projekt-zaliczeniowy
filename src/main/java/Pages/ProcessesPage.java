package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ProcessesPage extends HomePage {

    public ProcessesPage ( WebDriver driver ) {
        super ( driver );
    }

    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";

    @FindBy(css = "a[href*='/Projects/Create']")
    private WebElement addNewProcessBtn;


    @FindBy(css = ".title_left")
    private WebElement processesPageHeaderTitle;

    public CreateProcessPage goToCreateProcessPage ( ) {
        addNewProcessBtn.click ( );
        return new CreateProcessPage ( driver );
    }

    public ProcessesPage assertIfProcessesPageIsOpen ( ) {

        Assert.assertEquals ( driver.getCurrentUrl ( ), "http://localhost:4444/Projects", "this is not a processes page" );
        return this;
    }

    public ProcessesPage assertProcessIsNotShown ( String processName ) {
        String processXpath = String.format ( GENERIC_PROCESS_ROW_XPATH, processName );
        List<WebElement> process = driver.findElements ( By.xpath ( processXpath ) );
        Assert.assertEquals ( process.size ( ), 0 );
        return this;
    }


}
