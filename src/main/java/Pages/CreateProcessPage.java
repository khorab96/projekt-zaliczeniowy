package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CreateProcessPage extends ProcessesPage {

    public CreateProcessPage ( WebDriver driver ) {
        super ( driver );
    }


    @FindBy(id = "Name")
    private WebElement processNameTxt;

    @FindBy(id = "Description")
    private WebElement processDescriptionTxt;

    @FindBy(id = "Notes")
    private WebElement processNotesTxt;

    @FindBy(css = "input[type='submit']")
    private WebElement processCreateBtn;

    @FindBy(css = "span[class*='text-danger field-validation-error']")
    private WebElement nameTooShortOrLongError;

    @FindBy(css = "div>a[href*='/Projects']")
    private WebElement backToListBtn;

    public CreateProcessPage typeProcessName ( String processName ) {
        processNameTxt.clear ( );
        processNameTxt.sendKeys ( processName );
        return this;
    }

    public CreateProcessPage typeProcessDescription ( String processDescription ) {
        processDescriptionTxt.clear ( );
        processDescriptionTxt.sendKeys ( processDescription );
        return this;
    }

    public CreateProcessPage typeProcessNotes ( String processNotes ) {
        processNotesTxt.clear ( );
        processNotesTxt.sendKeys ( processNotes );
        return this;
    }

    public CreateProcessPage submitProcessFailure ( ) {
        processCreateBtn.click ( );
        return this;
    }

    public CreateProcessPage assertIfErrorAppeared ( String expMsg ) {
        Assert.assertTrue ( nameTooShortOrLongError.getText ( ).contains ( expMsg ) );
        return this;
    }

    public ProcessesPage gotBackToProcessesPage ( ) {
        backToListBtn.click ( );
        return new ProcessesPage ( driver );
    }
}
