import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

public class MenuNavigationTest extends SeleniumBaseTest {

    @Test
    public void menuNavigationTest ( ) {
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToProcesses ( )
                .assertIfProcessesPageIsOpen ( )
                .goToCharacteristics ( )
                .assertIfCharacteristicsPageIsOpen ( )
                .goToHomePage ( )
                .assertIfHomePageIsOpen ( );


    }

}
